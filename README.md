# Multi-Factor Authentication demo

## TOTP

### time based one time password

* a secret key is generated on a server, then it is exchanged to an application

* that application generates a time-based encryption of that secret key

* the time-based encryption is given to the server by a user

* the server validates the time-based token against the actual secret key using the time

* so, this it is a cheap way of doing symmetric shared-key cryptography


https://en.wikipedia.org/wiki/HMAC-based_one-time_password#parameters


https://en.wikipedia.org/wiki/Time-based_one-time_password


