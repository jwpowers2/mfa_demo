const { ObjectId } = require("mongoose");
const mongoose = require("mongoose");

const { Schema } = mongoose;

const MfaSchema = new Schema({
  user_id: {
    type: ObjectId,
    required: true
  },
  shared_key: {
    type: String,
    required: true
  },
  mfa_enabled: {
    type: Boolean,
    required: true
  }
});

const Mfa = mongoose.model("Mfa", MfaSchema);
module.exports = Mfa;
