let UserController = require("../controllers/UserController.js");

module.exports = app => {
  app.get("/", UserController.index);
  app.post("/login", UserController.login);
  app.post("/users", UserController.register);
  app.post("/token", UserController.token);
};
