let User = require("mongoose").model("User");
let Mfa = require("mongoose").model("Mfa");
let bcrypt = require("bcryptjs");
let qrcode = require("qrcode");
const crypto = require("crypto");
const { authenticator } = require("otplib");

authenticator.options = { crypto };

class UserController {
  index(req, res) {

    User.find()
      .then((users) => {
        res.render("index", { users: users });
      })
      .catch(e => {
        res.redirect("/");
      })
  }
  login(req, res) {
    console.log(req.body)
    if (!req.body.email || !req.body.password) return;
    User.findOne({ email: req.body.email })
      .then((user) => {

        bcrypt.compare(req.body.password, user.password, (error, result) => {
          console.log(result);
          if (result) {
            Mfa.findOne({ user_id: user._id }).then(mfaDoc => {
              console.log(mfaDoc);
              if (mfaDoc && mfaDoc.shared_key) {
                let newSec = mfaDoc.shared_key;
                const otpauth =
                  authenticator.keyuri(
                    encodeURIComponent(user),
                    encodeURIComponent("mfa-test"),
                    newSec
                  ) + "&chld=H|0";
                qrcode.toDataURL(otpauth, (err, imageUrl) => {
                  if (err) {
                    console.log(err);
                  } else {
                    res.render("mfa", { qrc: imageUrl });
                  }
                });
              }
            });
          } else {
            console.log("bad password");
          }
        });

      })
      .catch(e => {
        res.redirect("/");
      })
  }
  register(req, res) {
    if (!req.body.email || !req.body.password) return;
    let user = new User(req.body);
    bcrypt.hash(user.password, 10, (err, hashed_password) => {
      if ((user.password = hashed_password)) {
        try {
          user.save()
            .then(doc => {
              Mfa.findOne({ user_id: user.id }).then(duplicate => {
                if (!duplicate) {
                  const user_id = user._id;
                  const mfa_enabled = false;
                  const shared_key = authenticator.generateSecret();
                  const mfa = new Mfa({
                    user_id,
                    shared_key,
                    mfa_enabled
                  });
                  mfa
                    .save()
                    .then(() => {
                      res.redirect("/");
                    })
                    .catch(e => {
                      res.render("index", { errors: e });
                    });
                } else {
                  res.redirect("/");
                }
              });
            });
        } catch (e) {
          res.render("index", { errors: e });
        }

      }
    });
  }
  token(req, res) {
    const { email, password, totp } = req.body;
    User.findOne({ email: req.body.email }).then(user => {
      if (user) {
        Mfa.findOne({ user_id: user._id }).then(mfaDoc => {
          const isValid = authenticator.verify({
            secret: mfaDoc.shared_key,
            token: totp
          });
          if (isValid) {
            console.log("VALID");
            res.render("dashboard");
          } else {
            console.log("NOT VALID");
          }
        });
      }
    });
  }
}

module.exports = new UserController();
